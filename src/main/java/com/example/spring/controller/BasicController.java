package com.example.spring.controller;


import com.example.spring.model.Movie;
import com.example.spring.service.MovieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;


@Controller
public class BasicController {

    @Autowired
    private MovieService movieService;

    @RequestMapping("/")
    String holamundo(){
        return "index";
    }

    @RequestMapping("/add")
    String addTemplate(){
        return "addMovie";
    }

    @PostMapping("/add")
    String addMovie(@RequestParam String movie_name, Model model){
        //metodo para añadir una pelicula
        Movie m = new Movie();
        m.setMovie_name(movie_name);
        //guardar peli en base de datos
        movieService.addMovie(m);

        model.addAttribute("message","La película '" + movie_name + "' ha sido añadida.");

        //mostrar index
        return "index";
    }

    @RequestMapping("/delete")
    String deleteTemplade() {
        return "delete";
    }

    @DeleteMapping("/delete") //cuando se realiza una solicitud a la url cuando se hace delete
    String deleteMovie(@RequestParam Long movie_id, Model model ) {
        Movie m = movieService.getMovieById(movie_id);
        if (m == null) {
            model.addAttribute("mensaje", "la pelicula con id " + movie_id + " no existe.");
        } else {
            movieService.deleteMovie(movie_id);
            model.addAttribute("message", "La pelicula con id" + movie_id + "ha sido eliminado");
        }

        return "inicio";
    }

    @RequestMapping("/update")
    String updateTemplade() {
        return "updateMovie";
    }

    @PutMapping("/update")
        String updateMovie(@RequestParam Long movie_id, @RequestParam String movie_name, Model model) {
        Movie m = new Movie();
        m.setMovie_name(movie_name);

        //Comprobar si el id es de una peli
        Movie peli_existe = movieService.getMovieById(movie_id);
        if (peli_existe == null) {
            //enviar mensaje de que no existe
            model.addAttribute("mensaje", "La peli no existe ");
        } else {
            //Comprobar si el movie_name cambia
            if (movie_name.equals(peli_existe.getMovie_name())) {
                //mensaje de que no cambia el nombre de la peli
                model.addAttribute("mensaje", "no cambio el nombre de la pelicula ");

            } else {
                //Si cambia cambiarlos
                peli_existe.setMovie_name(movie_name);
                movieService.updateMovie(movie_id, m);
                //Añadir un mensaje de lo que ha pasado
                model.addAttribute("mensaje", "Se actualizó el nombre de la película");
            }
        }

        return "index";
    }
}

